class WikipediaSearch extends React.Component {

    constructor() {
        super();

        this.state = {
            results: [
                '', [], [], []
            ]
        };
    }

    render() {
        return(
            <div className="search-container">
                <SearchForm onSearch={this._handleSearch.bind(this)}/>
                <ListWiki results={this.state.results}/>
            </div>
        );
    }

    _handleSearch(search) {
        $.ajax({
            type: 'GET',
            url: apiUrl + search,
            jsonpCallback: 'jsonCallback',
            contentType: "application/json",
            dataType: 'jsonp',
            success: (data) => {
                this.setState({ results: data });
            },
            error: (error) => {
                console.log(error);
            }
        });
    }
}


class SingleWiki extends React.Component {

    render() {
        return (
            <div className="row">
                <a href={this.props.url} target="_blank" className="single-wiki" >
                    <h3>{this.props.title}</h3>
                    <p>{this.props.description}</p>
                </a>
            </div>
        )
    }
}

class ListWiki extends React.Component {

    render() {
        var results = this.props.results[1].map((result, index) => {
            return (
                <SingleWiki key={index} title={this.props.results[1][index]} description={this.props.results[2][index]} url={this.props.results[3][index]} />
            );
        });

        return (<div className="wiki-list">{results}</div>);
    }
}

class SearchForm extends React.Component {

    constructor() {
        super();
        this.state = {
            searchTerm: ''
        };
    }

    _handleInputChange(event) {
        this.setState({
            searchTerm: event.target.value
        });
    }

    _handleSubmit(event) {
        event.preventDefault();
        let searchTerm = this.state.searchTerm.trim();
        if (!searchTerm) {
            return;
        }
        this.props.onSearch(searchTerm);
        this.setState({ searchTerm: '' });
    }

    render() {
        return (
            <div className="row search-container" >
                <form onSubmit={this._handleSubmit.bind(this)}>
                    <div class="form-group">
                        <input className="search-box-text" type="text" placeholder="Wyszukiwana teza..." onChange={this._handleInputChange.bind(this)} value={this.state.searchTerm}/>
                        <button className="search" title="Search"  onClick={this._handleSubmit.bind(this)} >Wyszukaj</button>
                    </div>
                </form>
            </div>
        );
    }
}


let apiUrl = '//pl.wikipedia.org/w/api.php?action=opensearch&format=json&search=';

ReactDOM.render(<WikipediaSearch />, document.getElementById('wiki-search'));